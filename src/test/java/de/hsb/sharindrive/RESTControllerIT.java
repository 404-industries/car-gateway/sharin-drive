package de.hsb.sharindrive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
class RESTControllerIT {

    @Autowired
    MockMvc mockMvc;

//    @Test
//    void returnsNameWithBaseStatement() throws Exception {
//        mockMvc
//                .perform(get("/greeting/Seyling"))
//                .andExpect(status().isOk())
//                .andExpect(content().string("Guten Tag Seyling"));
//    }
//
//    @Test
//    void returnsNameWithExtraStatement() throws Exception {
//        mockMvc
//                .perform(get("/greeting/Athene"))
//                .andExpect(status().isOk())
//                .andExpect(content().string("Guten Tag wuensche ich Ihnen, Athene"));
//    }
}