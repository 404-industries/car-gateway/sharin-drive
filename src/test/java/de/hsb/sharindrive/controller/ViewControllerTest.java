package de.hsb.sharindrive.controller;

import de.hsb.sharindrive.exceptions.ZeitraumBelegtException;
import de.hsb.sharindrive.model.*;
import de.hsb.sharindrive.repository_interfaces.BenutzerRepository;
import de.hsb.sharindrive.service.BenutzerService;
import de.hsb.sharindrive.service.FahrzeugService;
import de.hsb.sharindrive.service.ReservierungsService;
import de.hsb.sharindrive.utils.HashUtils;
import de.hsb.sharindrive.utils.UserCheck;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ViewControllerTest {

    public ViewControllerTest() {
        id = 10;

        benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.ADMINISTRATOR);

        token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
    }

    private Benutzer benutzer;
    private int id;
    private String token;
    private Cookie[] cookies;

    @InjectMocks
    private ViewController viewController;

    @Spy
    private ModelAndView modelAndView;

    @Mock
    private UserCheck userCheck;

    @Mock
    private BenutzerRepository benutzerRepository;

    @Mock
    private BenutzerService benutzerService;

    @Mock
    private FahrzeugService fahrzeugService;

    @Mock
    private ReservierungsService reservierungsService;

    @Test
    void initBinder() {
        WebDataBinder binder = Mockito.mock(WebDataBinder.class);

        viewController.initBinder(binder);

//        Mockito.verify(binder, Mockito.times(1)).registerCustomEditor(Data.class, Mockito.isA(CustomDateEditor.class));
//        Mockito.verify(binder, Mockito.times(1)).registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @Test
    void welcome() {
//        ModelAndView modelAndView = Mockito.mock(ModelAndView.class);
        ModelAndView result = viewController.welcome();
//        Mockito.verify(modelAndView, Mockito.times(1)).setViewName("index");
        System.out.println("View Name " + result.getViewName());
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("index");
    }

    @Test
    void adminPage() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.ADMINISTRATOR);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.adminPage(request, model);

        assertThat(result).isNotNull();
        System.out.println("View Name " + result.getViewName());
        assertThat(result.getViewName()).isEqualTo("admin");

        benutzer.setRolle(Rollen.MITARBEITER);
        Mockito.when(userCheck.userHasRole(request, Rollen.ADMINISTRATOR)).thenReturn(true);
        try {
            result = viewController.adminPage(request, model);
            assert true;
        } catch (ResponseStatusException e) {
            assertThat(e.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void fahrzeugForm() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.fahrzeugForm(request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("fahrzeugForm");

        benutzer.setRolle(Rollen.MITARBEITER);
        result = viewController.fahrzeugForm(request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("fahrzeugForm");

        benutzer.setRolle(Rollen.GAST);
        Mockito.when(userCheck.userHasRole(request, Rollen.ADMINISTRATOR)).thenReturn(true);
        try {
            result = viewController.fahrzeugForm(request, model);
            assert true;
        } catch (ResponseStatusException e) {
            assertThat(e.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void postFahrzeug() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        Fahrzeug fahrzeug = new Fahrzeug();
        fahrzeug.setMarke("VW");

        String result = viewController.postFahrzeug(fahrzeug, model);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("resultFahrzeug");
        Mockito.verify(fahrzeugService, Mockito.times(1)).fahrzeugHinzufuegen(Mockito.isA(Fahrzeug.class));

        fahrzeug.setMarke(null);
        result = viewController.postFahrzeug(fahrzeug, model);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("resultFahrzeug");
    }

    @Test
    void benutzerPage() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.benutzerPage(request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("manageuser");

        benutzer.setRolle(Rollen.MITARBEITER);
        result = viewController.benutzerPage(request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("manageuser");

        benutzer.setRolle(Rollen.GAST);
        Mockito.when(userCheck.userHasRole(request, Rollen.ADMINISTRATOR)).thenReturn(true);
        try {
            result = viewController.benutzerPage(request, model);
            assert true;
        } catch (ResponseStatusException e) {
            assertThat(e.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void modifyBenutzer() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);
        Mockito.when(benutzerRepository.findById(id)).thenReturn(Optional.of(benutzer));

        ModelAndView result = viewController.modifyBenutzer(String.valueOf(id), request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("modifyBenutzer");

        benutzer.setRolle(Rollen.MITARBEITER);
        result = viewController.modifyBenutzer(String.valueOf(id), request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("modifyBenutzer");

        benutzer.setRolle(Rollen.GAST);
        Mockito.when(userCheck.userHasRole(request, Rollen.ADMINISTRATOR)).thenReturn(true);
        try {
            viewController.modifyBenutzer(String.valueOf(id), request, model);
            assert true;
        } catch (ResponseStatusException e) {
            assertThat(e.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void postBenutzerMod() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);
        Mockito.when(benutzerRepository.findById(id)).thenReturn(Optional.of(benutzer));

        String result = viewController.postBenutzerMod(benutzer, String.valueOf(id), model);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("result");
        Mockito.verify(benutzerService, Mockito.times(1)).benutzerHinzufuegen(Mockito.isA(Benutzer.class));

        benutzer.setEmail(null);
        result = viewController.postBenutzerMod(benutzer, String.valueOf(id), model);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("result");
    }

    @Test
    void benutzerProfile() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.ADMINISTRATOR);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.benutzerProfile(String.valueOf(id), request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("result");

        benutzer.setRolle(Rollen.GAST);
        Mockito.when(userCheck.userHasRole(request, Rollen.ADMINISTRATOR)).thenReturn(true);
        try {
            viewController.benutzerProfile(String.valueOf(id), request, model);
            assert true;
        } catch (ResponseStatusException e) {
            assertThat(e.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void fahrzeugePage() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.ADMINISTRATOR);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.fahrzeugePage(request, model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("manageFahrzeuge");

        benutzer.setRolle(Rollen.GAST);
        Mockito.when(userCheck.userHasRole(request, Rollen.ADMINISTRATOR)).thenReturn(true);
        try {
            viewController.fahrzeugePage(request, model);
            assert true;
        } catch (ResponseStatusException e) {
            assertThat(e.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        }
    }

    @Test
    void reservierungsForm() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Fahrzeug> fahrzeugList = new ArrayList<>();
//        benutzerList.add(benutzer);
        Mockito.when(fahrzeugService.getFahrzeuge()).thenReturn(fahrzeugList);

        ModelAndView result = viewController.reservierungsForm(String.valueOf(id), model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("reservierungsForm");

        result = viewController.reservierungsForm("d", model);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("reservierungsForm");
    }

    @Test
    void reserviereFahrzeug() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);

        ReservierungZwischenspeicher resEingabe= new ReservierungZwischenspeicher();
        resEingabe.setAusleihZeitpunkt("2022-10-15 15:00:00");
        resEingabe.setRueckgabeZeitpunkt("2022-10-16 15:00:00");
        resEingabe.setFahrzeugId(id);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Fahrzeug> fahrzeugList = new ArrayList<>();
        Fahrzeug fahrzeug = new Fahrzeug();
        fahrzeugList.add(fahrzeug);
        Mockito.when(fahrzeugService.getFahrzeuge()).thenReturn(fahrzeugList);

        ModelAndView result = viewController.reserviereFahrzeug(resEingabe, model, request);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("resultReservierung");
        try {
            Mockito.verify(reservierungsService, Mockito.times(1)).reservierungHinzufuegen(Mockito.isA(Reservierung.class));
        } catch (ZeitraumBelegtException e) {
            e.printStackTrace();
        }
    }

    @Test
    void löscheReservierung() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);
        MockHttpServletResponse response = new MockHttpServletResponse();

        Model model = new ExtendedModelMap();
        List<Reservierung> reservierungList = new ArrayList<>();
        Mockito.when(reservierungsService.getReservierungenNachKunde(id)).thenReturn(reservierungList);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.löscheReservierung(String.valueOf(id), model, request);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("manageReservierungen");
        Mockito.verify(reservierungsService, Mockito.times(1)).reservierungStornieren(id);
    }

    @Test
    void sieheReservierung() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);
        MockHttpServletResponse response = new MockHttpServletResponse();

        Model model = new ExtendedModelMap();
        List<Reservierung> reservierungList = new ArrayList<>();
//        benutzerList.add(benutzer);
        Mockito.when(reservierungsService.getReservierungenNachKunde(id)).thenReturn(reservierungList);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.sieheReservierung(model, request);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("manageReservierungen");
    }

    @Test
    void loginForm() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);
        MockHttpServletResponse response = new MockHttpServletResponse();

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.loginForm(model, response, request);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("resultLogin");

        cookies[1].setValue("222");
        result = viewController.loginForm(model, response, request);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("login");
    }

    @Test
    void postLogin() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(cookies);
        MockHttpServletResponse response = new MockHttpServletResponse();

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

//        List<Benutzer> benutzerList = new ArrayList<>();
//        benutzerList.add(benutzer);
        Benutzer benutzer1 = new Benutzer();
        benutzer1.setPw("123asdf");
        benutzer1.setEmail(benutzer.getEmail());
        Mockito.when(benutzerRepository.findByEmail(benutzer.getEmail())).thenReturn(benutzer1);
        String result = viewController.postLogin(benutzer, model, response);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("login");

        Mockito.when(benutzerRepository.findByEmail(benutzer.getEmail())).thenReturn(benutzer);

        result = viewController.postLogin(benutzer, model, response);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("resultLogin");

        Mockito.when(benutzerRepository.findByEmail(benutzer.getEmail())).thenReturn(null);
        result = viewController.postLogin(benutzer, model, response);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("login");

        benutzer.setEmail(null);
        Mockito.when(benutzer.getPw()).thenReturn(null);
        result = viewController.postLogin(benutzer, model, response);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("result");
    }

    @Test
    void logout() {
        HttpServletResponse response = new MockHttpServletResponse();

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        ModelAndView result = viewController.logout(model, response);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("index");
    }


    @Test
    void benutzerForm() {
        HttpServletResponse response = new MockHttpServletResponse();
//        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Fahrzeug> fahrzeugList = new ArrayList<>();
//        benutzerList.add(benutzer);
        Mockito.when(fahrzeugService.getFahrzeuge()).thenReturn(fahrzeugList);

        ModelAndView result = viewController.benutzerForm(model, response);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("BenutzerForm");

//        result = viewController.benutzerForm(model, response);
//        assertThat(result).isNotNull();
//        assertThat(result.getViewName()).isEqualTo("BenutzerForm");
    }

    @Test
    void postBenutzer() {
        HttpServletResponse response = new MockHttpServletResponse();
//        request.setCookies(cookies);

        Model model = new ExtendedModelMap();
        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);

        List<Fahrzeug> fahrzeugList = new ArrayList<>();
//        benutzerList.add(benutzer);
        Mockito.when(fahrzeugService.getFahrzeuge()).thenReturn(fahrzeugList);

        String result = viewController.postBenutzer(benutzer, model);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("result");
        Mockito.verify(benutzerService, Mockito.times(1)).benutzerHinzufuegen(Mockito.isA(Benutzer.class));

        benutzer.setLastname(null);
        result = viewController.postBenutzer(benutzer, model);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("result");
    }

    @Test
    void löscheBenutzer() {
        HttpServletResponse response = new MockHttpServletResponse();
//        request.setCookies(cookies);

//        Model model = new ExtendedModelMap();
//        Mockito.when(benutzerService.getBenutzer(id)).thenReturn(benutzer);
//        Mockito.when(benutzerRepository.deleteById(id)).thenReturn(benutzerList);

//        List<Fahrzeug> fahrzeugList = new ArrayList<>();
////        benutzerList.add(benutzer);
//        Mockito.when(fahrzeugService.getFahrzeuge()).thenReturn(fahrzeugList);

        ModelAndView result = viewController.löscheBenutzer(String.valueOf(id), response);
        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("index");

        Mockito.verify(benutzerRepository, Mockito.times(1)).deleteById(id);
    }
}
