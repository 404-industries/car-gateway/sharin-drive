package de.hsb.sharindrive.utils;

import de.hsb.sharindrive.model.Benutzer;
import de.hsb.sharindrive.model.Rollen;
import de.hsb.sharindrive.repository_interfaces.BenutzerRepository;
import de.hsb.sharindrive.service.BenutzerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.Cookie;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserCheckTest {

    @InjectMocks
    private UserCheck userCheck;

    @Mock
    private BenutzerService benutzerService;

    @Mock
    private BenutzerRepository benutzerRepository;

    @Test
    void validTokenCheck() {
        Benutzer benutzer = new Benutzer();
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        assertThat(UserCheck.validToken(benutzer, token)).isTrue();

        token = HashUtils.generateToken(benutzer.getFirstname(), "test", benutzer.getEmail(), benutzer.getPw());

        assertThat(UserCheck.validToken(benutzer, token)).isFalse();
    }

    @Test
    void userHasRoleCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.MITGLIEDER);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        boolean result = userCheck.userHasRole(request, Rollen.MITGLIEDER);
        assertThat(result).isTrue();
    }

    @Test
    void userHasRoleNotCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.MITGLIEDER);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        boolean result = userCheck.userHasRole(request, Rollen.ADMINISTRATOR);
        assertThat(result).isFalse();
    }

    @Test
    void userHasAnyRoleCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.MITGLIEDER);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        boolean result = userCheck.userHasAnyRole(request);
        assertThat(result).isTrue();
    }

    @Test
    void userHasAnyRoleNotCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");
        benutzer.setRolle(Rollen.GAST);

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        boolean result = userCheck.userHasAnyRole(request);
        assertThat(result).isFalse();
    }

    @Test
    void validUserLoggedInCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        boolean result = userCheck.validUserLoggedIn(request);
        assertThat(result).isTrue();
    }

    @Test
    void validUserLoggedInNullCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;
        String token = "token";
        Cookie[] cookies = null;

        request.setCookies(cookies);

        assertThat(userCheck.validUserLoggedIn(request)).isFalse();
    }

    @Test
    void getUserFromTokenCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;

        Benutzer benutzer = new Benutzer();
        benutzer.setId(id);
        benutzer.setFirstname("Joel");
        benutzer.setLastname("Nope");
        benutzer.setEmail("Joel@lols.de");
        benutzer.setPw("123456");

        String token = HashUtils.generateToken(benutzer.getFirstname(), benutzer.getLastname(), benutzer.getEmail(), benutzer.getPw());

        Cookie[] cookies = new Cookie[2];
        cookies[0] = new Cookie("id", "" + id);
        cookies[1] = new Cookie("token", token);
        request.setCookies(cookies);

        List<Benutzer> benutzerList = new ArrayList<>();
        benutzerList.add(benutzer);
        Mockito.when(benutzerRepository.findAll()).thenReturn(benutzerList);

        Benutzer benutzer2 = userCheck.getUserFromToken(request);
        System.out.println(benutzer2);
        assertThat(benutzer2).isNotNull();
    }

    @Test
    void getUserFromTokenNullCheck() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        int id = 10;
        String token = "token";
        Cookie[] cookies = null;

        request.setCookies(cookies);

        Benutzer benutzer2 = userCheck.getUserFromToken(request);
        assertThat(benutzer2).isNull();
    }
}
