package de.hsb.sharindrive.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class HashUtilsTest {

    @InjectMocks
    private HashUtils hashUtils;

    @Test
    void getByteHash() {
        String value = "Hello";
        byte[] bytes = HashUtils.getByteHash(value);
        assertThat(bytes).isNotNull();
        assertThat(bytes).isNotEmpty();
//        for (byte b:bytes)
//            System.out.println(b);
    }

    @Test
    void bytesToHex() {
        byte[] bytes = {1,1,1,1,1,1,1,1};
        String result = HashUtils.bytesToHex(bytes);
        assertThat(result).isEqualTo("0101010101010101");
    }

    @Test
    void generateToken() {
        String value = "Hello";
        String result = HashUtils.generateToken(value, value, value, value);
//        System.out.println(result);;
        assertThat(result).isEqualTo("5949062927bdcb5f9207c9a1e0108620c3c8f72b76325e1fa4cf0d4ffca41e45");
    }

}
