package de.hsb.sharindrive.utils;

import de.hsb.sharindrive.model.Benutzer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@ExtendWith(MockitoExtension.class)
public class CookiesTest {

    @InjectMocks
    private Cookies cookies;

    @Mock
    private Benutzer benutzer;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Test
    void setCookiesTest() {
        Cookies.setCookies(benutzer, httpServletResponse);
        Mockito.verify(httpServletResponse, Mockito.times(2)).addCookie(Mockito.isA(Cookie.class));
    }

    @Test
    void deleteCookiesTest() {
        Cookies.deleteCookies(httpServletResponse);
        Mockito.verify(httpServletResponse, Mockito.times(2)).addCookie(Mockito.isA(Cookie.class));
    }
}
