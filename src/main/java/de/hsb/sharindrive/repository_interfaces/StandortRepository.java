package de.hsb.sharindrive.repository_interfaces;

import de.hsb.sharindrive.model.Standort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StandortRepository extends JpaRepository <Standort, Double>
{

}
