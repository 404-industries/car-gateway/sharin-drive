package de.hsb.sharindrive.repository_interfaces;

import de.hsb.sharindrive.model.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<Adresse, Long>
{

}
