package de.hsb.sharindrive.repository_interfaces;

import de.hsb.sharindrive.model.Reservierung;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservierungRepository extends JpaRepository <Reservierung, Long>
{
    public List<Reservierung> findByKunde(Integer kundenId);
    public List<Reservierung> findByFahrzeug(Long fahrzeugId);
}
