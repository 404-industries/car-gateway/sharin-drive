package de.hsb.sharindrive.repository_interfaces;

import de.hsb.sharindrive.model.Fahrzeug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FahrzeugRepository  extends JpaRepository<Fahrzeug, Long>
{

}
