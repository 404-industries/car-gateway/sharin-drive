package de.hsb.sharindrive.repository_interfaces;

import de.hsb.sharindrive.model.Benutzer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BenutzerRepository extends JpaRepository<Benutzer, Integer> {
    Benutzer findByEmail(String email);
}

