package de.hsb.sharindrive.controller;

import de.hsb.sharindrive.exceptions.IdNonExistantException;
import de.hsb.sharindrive.exceptions.IllegalZeitraumException;
import de.hsb.sharindrive.exceptions.ZeitraumBelegtException;
import de.hsb.sharindrive.model.*;
import de.hsb.sharindrive.repository_interfaces.BenutzerRepository;
import de.hsb.sharindrive.service.BenutzerService;
import de.hsb.sharindrive.service.FahrzeugService;
import de.hsb.sharindrive.service.ReservierungsService;
import de.hsb.sharindrive.utils.Cookies;
import de.hsb.sharindrive.utils.HashUtils;
import de.hsb.sharindrive.utils.UserCheck;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class ViewController {

// docker run -p 5432:5432 -e POSTGRES_USER=dbuser -e POSTGRES_PASSWORD=123456 -e POSTGRES_DB=carshare postgres

    private final BenutzerRepository repository;
    private final FahrzeugService fahrzeugService;
    private final ReservierungsService reservierungsService;
    private final BenutzerService benutzerService;

    private String tempPw;

    private UserCheck userCheck;

    public ViewController(BenutzerRepository repo, FahrzeugService fahrzeugService, ReservierungsService reservierungsService, BenutzerService benutzerService){
        this.repository = repo;
        this.fahrzeugService = fahrzeugService;
        this.reservierungsService = reservierungsService;
        this.benutzerService = benutzerService;
        userCheck = new UserCheck(repository);
    }

    //Wird verwendet für die Darstellung vom Datum im Mod Benutzer
    @InitBinder
    public void initBinder (WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping("/")
    public ModelAndView welcome() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }

    /*
    Aber hier nur das für Administrative nutzer
     */

    @GetMapping("/admin")
    public ModelAndView adminPage(HttpServletRequest request, Model model) {
        ModelAndView mav = new ModelAndView();

        if (userCheck.userHasRole(request, Rollen.ADMINISTRATOR)) {
            mav.setViewName("admin");
            model.addAttribute("benutzer", new Benutzer());
            mav.addObject(model);
            return mav;
        }
//        return mav;
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    /*
    Aber hier das für Mitarbeiter
     */

    @GetMapping("/fahrzeugadd")
    public ModelAndView fahrzeugForm(HttpServletRequest request, Model model) {
        ModelAndView mav = new ModelAndView();
        if (userCheck.userHasRole(request, Rollen.ADMINISTRATOR) || userCheck.userHasRole(request, Rollen.MITARBEITER)) {
            mav.setViewName("fahrzeugForm");
            model.addAttribute("fahrzeug", new Fahrzeug());
            mav.addObject(model);
            return mav;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/fahrzeugadd")
    public String postFahrzeug(@Valid @ModelAttribute("fahrzeug") Fahrzeug fahrzeug, Model model){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("resultFahrzeug");
        if (fahrzeug.getMarke() != null){
            model.addAttribute("fahrzeug",fahrzeug);
            mav.addObject(model);
            fahrzeugService.fahrzeugHinzufuegen(fahrzeug);
            return "resultFahrzeug";
        }
        else {
            return "resultFahrzeug";
        }
    }

    @GetMapping("/listb")
    public ModelAndView benutzerPage(HttpServletRequest request, Model model){
        ModelAndView mav = new ModelAndView();
        if (userCheck.userHasRole(request, Rollen.ADMINISTRATOR) || userCheck.userHasRole(request, Rollen.MITARBEITER)) {
            mav.setViewName("manageuser");
            model.addAttribute("listBenutzer", repository.findAll());
            mav.addObject(model);
            return mav;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/benutzermod")
    public ModelAndView modifyBenutzer(@RequestParam(name="id") String id, HttpServletRequest request, Model model){
        ModelAndView mav = new ModelAndView();
        if (userCheck.userHasRole(request, Rollen.ADMINISTRATOR) || userCheck.userHasRole(request, Rollen.MITARBEITER)) {
            mav.setViewName("modifyBenutzer");
//            System.out.println(repository.findById(Integer.parseInt(id)).get());
            Benutzer b = repository.findById(Integer.parseInt(id)).get();
            tempPw = b.getPw();
//            System.out.println("Altes PW: "+tempPw);
            model.addAttribute("benutzer", b);
            mav.addObject(model);
            return mav;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/benutzermod")
    public String postBenutzerMod(@Valid @ModelAttribute("benutzer") Benutzer benutzer, @RequestParam(name="id") String id, Model model){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("result");
        if (benutzer.getEmail() != null){
            benutzer.setId(Integer.parseInt(id));
//            System.out.println("Benutzer übergebene PW: "+benutzer.getPw());
//            System.out.println("Berechnetes leeres PW: "+HashUtils.bytesToHex(HashUtils.getByteHash("")));
            if(HashUtils.bytesToHex(HashUtils.getByteHash("")).equals(benutzer.getPw())){
                benutzer.setHashPw(tempPw);
            }
            model.addAttribute("benutzer",benutzer);
            mav.addObject(model);
            benutzerService.benutzerHinzufuegen(benutzer);
            tempPw = null;
            return "result";
        }
        else {
            return "result";
        }
    }

    /*
    Aber hier nur das für Mitglieder
     */

    @GetMapping("/profile")
    public ModelAndView benutzerProfile(@RequestParam(name="id") String id, HttpServletRequest request, Model model){
        ModelAndView mav = new ModelAndView();
        if (userCheck.userHasAnyRole(request)) {
            mav.setViewName("result");
            model.addAttribute("benutzer", benutzerService.getBenutzer(Integer.parseInt(id)));
            mav.addObject(model);
            return mav;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/listf")
    public ModelAndView fahrzeugePage(HttpServletRequest request, Model model){
        ModelAndView mav = new ModelAndView();
        if (userCheck.userHasAnyRole(request)) {
            mav.setViewName("manageFahrzeuge");
            model.addAttribute("listFahrzeuge", fahrzeugService.getFahrzeuge());
            mav.addObject(model);
            return mav;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/reserFahr")
    public ModelAndView reservierungsForm(@RequestParam(name="id", required=true) String id, Model model)
    {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("reservierungsForm");
        try
        {
            long fahrzeugId = Long.parseLong(id);

            model.addAttribute("resEingabe", new ReservierungZwischenspeicher(fahrzeugId));
            mav.addObject(model);
        }catch(NumberFormatException e)
        {

        }
            return mav;
    }

    @PostMapping("/reserFahr")
    public ModelAndView reserviereFahrzeug(@Valid @ModelAttribute("resEingabe") ReservierungZwischenspeicher resEingabe, Model model, HttpServletRequest request)
    {
        try
        {
            Benutzer benutzer = userCheck.getUserFromToken(request);

//            System.out.println("die FahrzeugID ist "+resEingabe.getFahrzeugId()+" und das fahrzeug heißt "+fahrzeugService.getFahrzeug(resEingabe.getFahrzeugId()).getMarke());
            Reservierung reservierung = new Reservierung(Timestamp.valueOf(resEingabe.getAusleihZeitpunkt()),
                                                            Timestamp.valueOf(resEingabe.getRueckgabeZeitpunkt()),
                                                            fahrzeugService.getFahrzeug(resEingabe.getFahrzeugId()),
                                                            (userCheck.getUserFromToken(request)));
            reservierungsService.reservierungHinzufuegen(reservierung);
            model.addAttribute("reservierung", reservierung);
        }
        catch(IllegalZeitraumException | ZeitraumBelegtException e)
        {
            //hier muss noch eine nachricht rein die geschickt wird wenn der zeitraum belegt ist
        } catch (IdNonExistantException e)
        {

        }
        ModelAndView mav = new ModelAndView();
        mav.setViewName("resultReservierung");
        mav.addObject(model);
        return mav;
    }

    @GetMapping("/delReserv")
    public ModelAndView löscheReservierung(@RequestParam(name="id", required=true) String id, Model model, HttpServletRequest request)
    {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("manageReservierungen");
        mav.addObject(model);
        try
        {
            long reservierungsId = Long.parseLong(id);
            int kundenId = userCheck.getUserFromToken(request).getId();
            reservierungsService.reservierungStornieren(Long.parseLong(id));
            List<Reservierung> r = reservierungsService.getReservierungenNachKunde(Integer.parseInt(id));
            model.addAttribute("reservierungen", r);
        }
        catch(NumberFormatException e)
        {

        }
            return mav;
    }

    @GetMapping("/viewR")
    public ModelAndView sieheReservierung(Model model, HttpServletRequest request){
        int userId = userCheck.getUserFromToken(request).getId();

        List<Reservierung> r = reservierungsService.getReservierungenNachKunde(userId);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("manageReservierungen");
        model.addAttribute("reservierungen", r);
        mav.addObject(model);
        return mav;
    }


    /*Aber hier nur das für alle nutzer
     */

    @GetMapping("/login")
    public ModelAndView loginForm(Model model, HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (userCheck.validUserLoggedIn(request)) {
            Benutzer benutzer = userCheck.getUserFromToken(request);
            mav.setViewName("resultLogin");
            model.addAttribute("benutzer", benutzer);
        } else {
            mav.setViewName("login");
            model.addAttribute("benutzer", new Benutzer());
        }
        mav.addObject(model);
        return mav;
    }

    @PostMapping("/login")
    public String postLogin(@Valid @ModelAttribute("benutzer") Benutzer benutzer, Model model, HttpServletResponse response){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("resultLogin");
        if (benutzer.getEmail() != null && benutzer.getPw() != null){
            Benutzer liste = repository.findByEmail(benutzer.getEmail());
//            System.out.println("Ist Liste leer: "+ liste);
            if(liste != null) {
                if (liste.getPw().equals(benutzer.getPw())){
//                    System.out.println("Anmelden...");
//                    System.out.println(liste);
                    model.addAttribute("benutzer", liste);
                    mav.addObject(model);

                    Cookies.setCookies(liste, response);
                    return "resultLogin";
                }
                else {
//                    System.out.println("Falsches Passwort");
                    return "login";
                }
            }else{
//                System.out.println("Kein valider Benutzer");
                return "login";
            }
        } else {
            return "result";
        }
    }

    @GetMapping("/logout")
    public ModelAndView logout(Model model, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        mav.addObject(model);

        Cookies.deleteCookies(response);

        return mav;
    }

    @GetMapping("/benutzeradd")
    public ModelAndView benutzerForm(Model model, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("BenutzerForm");
        model.addAttribute("benutzer", new Benutzer());
        mav.addObject(model);

//        Cookies.deleteCookies(response);

        return mav;
    }

    @PostMapping("/benutzeradd")
    public String postBenutzer(@Valid @ModelAttribute("benutzer") Benutzer benutzer, Model model){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("result");
        if (benutzer.getLastname() != null){
            model.addAttribute("benutzer",benutzer);
            mav.addObject(model);
            //repository.save(benutzer);
            benutzerService.benutzerHinzufuegen(benutzer);
            return "result";
        }
        else {
            return "result";
        }
    }

    @GetMapping("/delBen")
    public ModelAndView löscheBenutzer(@RequestParam(name="kundenId", required=true) String kId,HttpServletResponse response) {
        repository.deleteById(Integer.parseInt(kId));
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        Cookies.deleteCookies(response);
        return mav;
    }
}
