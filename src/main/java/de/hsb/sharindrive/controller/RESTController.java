package de.hsb.sharindrive.controller;

import org.springframework.web.bind.annotation.*;


@RestController
public class RESTController {
/*
   private final BenutzerRepository repository;

    public RESTController(BenutzerRepository repo, FahrzeugRepository frepo, ReservierungRepository rrepo){
        this.repository = repo;
    }


    @GetMapping("/benutzer")
    public List<Benutzer> getBenutzer() {
        return repository.findAll();
    }
/*

    @GetMapping("/fahrzeugo")
    public List<Fahrzeug> getFahrzeug() {
        return .findAll();
    }

    @GetMapping("/resero")
    public List<Reservierung> getReservierungen() {
        return reservierungRepository.findAll();
    }

    @GetMapping("/login/{id}")
    public String setCookie(HttpServletResponse response, @PathVariable("id") int id) {
        // create a cookie
        if (repository.existsById(id)) {
            Benutzer benutzer = repository.getById(id);
            Cookies.setCookies(benutzer, response);
            return "Updated Cookies!";
        }
        return "Nothing found";
    }

//    @GetMapping("/adminTest")
//    public String adminPage(HttpServletRequest request) {
//        Rollen requiredRole = Rollen.ADMINISTRATOR;
//
//        Cookie[] cookies = request.getCookies();
//        if (cookies != null) {
//            for (Cookie cookie : cookies) {
//                System.out.println(cookie.getValue());
//            }
//            int id = Integer.parseInt(cookies[0].getValue());
//            if (repository.existsById(id)) {
//                Benutzer benutzer = repository.getById(id);
//                return UserCheck.validUserAccess(benutzer, id, cookies[1].getValue(), Rollen.ADMINISTRATOR) + "";
//            }
//        }
//        return "Error";
//    }

//    @PostMapping("/loginOH")
//    @ResponseBody
//    public ResponseEntity<String> pwUeberpruefen(@RequestBody Benutzer benutzer){
//        if (benutzer.getEmail() != null && benutzer.getPw() != null){
//            List<Benutzer> liste =repository.findByEmail(benutzer.getEmail());
//
//            if(liste.get(0).getPw().equals(benutzer.getPw())){
//                return ResponseEntity.status(HttpStatus.OK).body("Korrenktes Passwort");
//            }else{
//                return ResponseEntity.status(HttpStatus.OK).body("Falsches Passwort");
//            }
//        } else {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
//        }
//    }

    @PostMapping("/benutzer")
    @ResponseBody
    public ResponseEntity<String> postOhnePageBenutzer(@Valid @RequestBody Benutzer benutzer) {
        if (benutzer.getLastname() != null) {
            repository.save(benutzer);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping("/fahrzeugo")
    @ResponseBody
    public ResponseEntity<String> postOhnePageFahrzeug(@Valid @RequestBody Fahrzeug fahrzeug) {
        if (fahrzeug.getMarke() != null) {
            fahrzeugRepository.save(fahrzeug);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }


    @PostMapping("/resero")
    @ResponseBody
    public ResponseEntity<String> postReservierungO(@Valid @RequestBody Reservierung reservierung) {
        if (reservierung.getReservierungsDatum() == null) {
            reservierung.setReservierungsDatum(new Date());
            reservierungRepository.save(reservierung);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        //reservierung.getKundeReservieungId() != null &&
    }



    @DeleteMapping("/benutzer")
    public void loescheAlleBenutzer(){
        repository.deleteAll();
    }

    @DeleteMapping("/fahrzeugo")
    public void loescheAlleFahrzeuge(){
        fahrzeugRepository.deleteAll();
    }

*/
}
