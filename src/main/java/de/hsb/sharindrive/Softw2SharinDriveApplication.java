package de.hsb.sharindrive;

import de.hsb.sharindrive.model.Benutzer;
import de.hsb.sharindrive.model.Rollen;
import de.hsb.sharindrive.repository_interfaces.BenutzerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Softw2SharinDriveApplication {

	public static void main(String[] args) {
		SpringApplication.run(Softw2SharinDriveApplication.class, args);
	}

	@Bean
	public CommandLineRunner demoData(BenutzerRepository repo) {
		return args -> {
			if (repo.count() == 0) {
				Benutzer benutzer = new Benutzer();
				benutzer.setFirstname("admin");
				benutzer.setLastname("admin");
				benutzer.setEmail("admin@admin.de");
				benutzer.setPlz(12345);
				benutzer.setAdr("Admin Street 1");
				benutzer.setRolle(Rollen.ADMINISTRATOR);
				benutzer.setPw("admin");

				repo.save(benutzer);
			}
		};
	}
}
