package de.hsb.sharindrive.utils;

import de.hsb.sharindrive.model.Benutzer;
import de.hsb.sharindrive.model.Rollen;
import de.hsb.sharindrive.repository_interfaces.BenutzerRepository;
import de.hsb.sharindrive.service.BenutzerService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class UserCheck {

    private final BenutzerRepository benutzerRepository;
    private BenutzerService benutzerService;

    public UserCheck(BenutzerRepository benutzerRepository) {
        this.benutzerRepository = benutzerRepository;
        benutzerService = new BenutzerService(benutzerRepository);
    }

    public static boolean validToken(Benutzer benutzer, String token) {
        String tokenGeneratedFromDatabase = HashUtils.generateToken(
                benutzer.getFirstname(),
                benutzer.getLastname(),
                benutzer.getEmail(),
                benutzer.getPw()
        );

        if (token.equals(tokenGeneratedFromDatabase)) {
            return true;
        }
        return false;
    }

    public boolean userHasRole(HttpServletRequest request, Rollen rolle) {
        if (validUserLoggedIn(request) && rolle.equals(getUserFromToken(request).getRolle())) {
            return true;
        }
        return false;
    }

    public boolean userHasAnyRole(HttpServletRequest request) {

        if (validUserLoggedIn(request)) {
            Benutzer benutzer = getUserFromToken(request);
            return benutzer.getRolle().equals(Rollen.ADMINISTRATOR) ||
                    benutzer.getRolle().equals(Rollen.MITARBEITER) ||
                    benutzer.getRolle().equals(Rollen.MITGLIEDER);
        }
        return false;
    }

    public boolean validUserLoggedIn(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        int id = 0;
        String token = "";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("id"))
                    id = Integer.parseInt(cookie.getValue());
                else if (cookie.getName().equals("token"))
                    token = cookie.getValue();
            }
            Benutzer benutzer = benutzerService.getBenutzer(id);
            if (benutzer != null && validToken(benutzer, token)) {
                return true;
            }
        }
        return false;
    }

    public Benutzer getUserFromToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        int id = 0;
        String token = "";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("id"))
                    id = Integer.parseInt(cookie.getValue());
                else if (cookie.getName().equals("token"))
                    token = cookie.getValue();
            }
            Benutzer benutzer = benutzerService.getBenutzer(id);
            if (benutzer != null && validToken(benutzer, token)) {
                return benutzer;
            }
        }
        return null;
    }
}
