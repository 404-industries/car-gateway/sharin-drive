package de.hsb.sharindrive.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtils {

    public static byte[] getByteHash(String string){
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(string.getBytes(StandardCharsets.UTF_8));
        }
        catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return new byte[0];
    }

    public static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String generateToken(String firstname, String lastname, String email, String passwordHash) {
        return bytesToHex(getByteHash(firstname + lastname + email + passwordHash));
    }

}
