package de.hsb.sharindrive.utils;

import de.hsb.sharindrive.model.Benutzer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class Cookies {

    public static void setCookies(Benutzer benutzer, HttpServletResponse response) {
        Cookie[] cookies = new Cookie[2];

        String token = HashUtils.generateToken(
                benutzer.getFirstname(),
                benutzer.getLastname(),
                benutzer.getEmail(),
                benutzer.getPw()
        );

        cookies[0] = new Cookie("id", "" + benutzer.getId());
        cookies[1] = new Cookie("token", token);

        for (Cookie cookie : cookies) {
            cookie.setSecure(true);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
    }

    public static void deleteCookies(HttpServletResponse response) {
        Cookie[] cookies = new Cookie[2];

        cookies[0] = new Cookie("id", "" + null);
        cookies[1] = new Cookie("token", null);

        for (Cookie cookie : cookies) {
            cookie.setMaxAge(0);
            cookie.setSecure(true);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
    }
}
