package de.hsb.sharindrive.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "adresse")
public class Adresse
{
    @Id
    @TableGenerator(name = "adressID", initialValue = -1, allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "adressID"
    )
    private Long id;//primärschlüssel
    //die folgenden alle not null
    private String straße;
    private int hausnummer;
    private int postleitzahl;
    private String bundesland;

    @OneToMany(mappedBy = "adresse")
    private Set<Standort> standort;

    public Adresse(String straße, int hausnummer, int postleitzahl, String bundesland, Set<Standort> standort) {
        this.straße = straße;
        this.hausnummer = hausnummer;
        this.postleitzahl = postleitzahl;
        this.bundesland = bundesland;
        this.standort = standort;
    }


    /*__________get und set__________*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStraße() {
        return straße;
    }

    public void setStraße(String straße) {
        this.straße = straße;
    }

    public int getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(int hausnummer) {
        this.hausnummer = hausnummer;
    }

    public int getPostleitzahl() {
        return postleitzahl;
    }

    public void setPostleitzahl(int postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public String getBundesland() {
        return bundesland;
    }

    public void setBundesland(String bundesland) {
        this.bundesland = bundesland;
    }
}
