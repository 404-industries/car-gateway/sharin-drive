package de.hsb.sharindrive.model;

import javax.persistence.*;

@Entity
@Table(name = "standort")
public class Standort
{
    @Id
    @TableGenerator(name = "standortID", initialValue = -1, allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "standortID"
    )
    private long id;

    private double breitengrad;
    private double laengengrad;

    @ManyToOne(fetch = FetchType.LAZY) //hier bin ich mir nicht ganz sicher, weil eine adresse ja mehrere standorte für autos haben kann
    @JoinColumn(name = "adresse_id")
    private Adresse adresse; //fremdschlüssel auf adresse

    @OneToOne
    @JoinColumn(name = "fahrzeug_id")
    private Fahrzeug fahrzeug;

    public Standort(double breitengrad, double laengengrad) {
        this.breitengrad = breitengrad;
        this.laengengrad = laengengrad;
    }

    /*__________get und set__________*/

    public double getBreitengrad() {
        return breitengrad;
    }

    public void setBreitengrad(double breitengrad) {
        this.breitengrad = breitengrad;
    }

    public double getLaengengrad() {
        return laengengrad;
    }

    public void setLaengengrad(double laengengrad) {
        this.laengengrad = laengengrad;
    }
}
