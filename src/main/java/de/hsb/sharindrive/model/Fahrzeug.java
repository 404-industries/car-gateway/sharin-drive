package de.hsb.sharindrive.model;

import javax.persistence.*;

@Entity
@Table(name = "fahrzeug")
public class Fahrzeug
{
    @Id
    @TableGenerator(name = "fahrzeugID", initialValue = -1, allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "fahrzeugID"
    )
    private long id;

    //alles hier eigentlich not null
    private String marke;
    private String modell;
    private String groesse;
    private String treibstoffArt;
    private String fahrzeugFotoURL;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "standort_id", referencedColumnName = "id")
    private Standort standort;


    public Fahrzeug(){}

    public Fahrzeug(String marke,
                    String modell,
                    String groesse,
                    String treibstoffArt,
                    String fahrzeugFotoURL,
                    Standort standort)
    {
        this.marke = marke;
        this.modell = modell;
        this.groesse = groesse;
        this.treibstoffArt = treibstoffArt;
        this.fahrzeugFotoURL = fahrzeugFotoURL;
        this.standort = standort;
    }

    public Fahrzeug(String marke,
                    String modell,
                    String groesse,
                    String treibstoffArt,
                    String fahrzeugFotoURL)
    {
        this.marke = marke;
        this.modell = modell;
        this.groesse = groesse;
        this.treibstoffArt = treibstoffArt;
        this.fahrzeugFotoURL = fahrzeugFotoURL;
    }


    /*_______________getter und setter________________*/

    public Standort getStandort() {
        return standort;
    }

    public void setStandort(Standort standort) {
        this.standort = standort;
    }

    public String getGroesse() {
        return groesse;
    }

    public void setGroesse(String groesse) {
        this.groesse = groesse;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    public String getTreibstoffArt() {
        return treibstoffArt;
    }

    public void setTreibstoffArt(String treibstoffArt) {
        this.treibstoffArt = treibstoffArt;
    }


    public String getFahrzeugFotoURL() {
        return fahrzeugFotoURL;
    }

    public void setFahrzeugFotoURL(String fahrzeugFotoURL) {
        this.fahrzeugFotoURL = fahrzeugFotoURL;
    }
}
