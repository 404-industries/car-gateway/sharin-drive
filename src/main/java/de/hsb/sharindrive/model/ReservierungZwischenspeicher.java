package de.hsb.sharindrive.model;

public class ReservierungZwischenspeicher
{
    private String ausleihZeitpunkt;
    private String rueckgabeZeitpunkt;
    private long fahrzeugId;

    public ReservierungZwischenspeicher(String ausleihZeitpunkt, String rueckgabeZeitpunkt, long fahrzeugId) {
        this.ausleihZeitpunkt = ausleihZeitpunkt;
        this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
        this.fahrzeugId = fahrzeugId;
    }

    public ReservierungZwischenspeicher() {

    }
    public ReservierungZwischenspeicher(long fahrzeugId) {
        this.fahrzeugId = fahrzeugId;
    }

    public String getAusleihZeitpunkt() {
        return ausleihZeitpunkt;
    }

    public void setAusleihZeitpunkt(String ausleihZeitpunkt) {
        this.ausleihZeitpunkt = ausleihZeitpunkt;
    }

    public String getRueckgabeZeitpunkt() {
        return rueckgabeZeitpunkt;
    }

    public void setRueckgabeZeitpunkt(String rueckgabeZeitpunkt) {
        this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
    }

    public long getFahrzeugId() {
        return fahrzeugId;
    }

    public void setFahrzeugId(long fahrzeugId) {
        this.fahrzeugId = fahrzeugId;
    }
}
