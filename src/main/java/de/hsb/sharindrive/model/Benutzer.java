package de.hsb.sharindrive.model;

import de.hsb.sharindrive.utils.HashUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "benutzer")
public class Benutzer {

    private @GeneratedValue @Id Integer  id;
    private String firstname;
    private String lastname;
    @NotNull
    private String email;
    private Integer plz;
    private String adr;
    private Rollen rolle = Rollen.MITGLIEDER;
    private String pw;
    private Date geburtstagDatum;

    public Rollen getRolle() {
        return rolle;
    }

    public void setRolle(Rollen rolle) {
        this.rolle = rolle;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {return email;}

    public void setEmail(String email) {this.email = email;}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlz() { return plz; }

    public void setPlz(Integer plz) { this.plz = plz; }

    public String getAdr() { return adr; }

    public void setAdr(String adr) { this.adr = adr; }

    public String getPw() { return pw; }

    public void setHashPw(String pw){this.pw = pw;}

    public void setPw(String pw)  {
        this.pw = HashUtils.bytesToHex(HashUtils.getByteHash(pw));
    }

    public Date getGeburtstagDatum() {
        return geburtstagDatum;
    }

    public void setGeburtstagDatum(String geburtstagDatum) {
        try{
            System.out.println(geburtstagDatum);
            this.geburtstagDatum = new SimpleDateFormat("yyyy-MM-dd").parse(geburtstagDatum);
        }
        catch (ParseException e){
            e.printStackTrace();
        }
    }

}
