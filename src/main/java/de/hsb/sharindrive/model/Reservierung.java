package de.hsb.sharindrive.model;

import de.hsb.sharindrive.exceptions.IllegalZeitraumException;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "reservierung")
public class Reservierung
{
    @Id
    @TableGenerator(name = "reservierungsID", initialValue = -1, allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "reservierungsID"
    )
    private Long id;

    private Date reservierungsDatum; //automatisch generieren, zum datum der erstellung
    private Timestamp ausleihZeitpunkt;
    private Timestamp rueckgabeZeitpunkt;

    @OneToOne
    @JoinColumn(name = "fahrzeug_id", referencedColumnName = "id")
    private Fahrzeug fahrzeug; //fremdschlüssel auf fahrzeug

    @OneToOne
    @JoinColumn(name = "kunden_id", referencedColumnName = "id")
    private Benutzer kunde;   //fremdschlüssel auf kunde/benutzer

    public Reservierung(){}

    public Reservierung(Timestamp ausleihZeitpunkt,
                        Timestamp rueckgabeZeitpunkt,
                        Fahrzeug fahrzeug,
                        Benutzer kunde)
            throws IllegalZeitraumException
    {
        if(ausleihZeitpunkt.after(rueckgabeZeitpunkt))
            throw new IllegalZeitraumException();
        else
        {
            this.reservierungsDatum = new Date(System.currentTimeMillis());
            this.ausleihZeitpunkt = ausleihZeitpunkt;
            this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
            this.fahrzeug = fahrzeug;
            this.kunde = kunde;
        }
    }

    public Reservierung(Timestamp ausleihZeitpunkt,
                        Timestamp rueckgabeZeitpunkt,
                        Fahrzeug fahrzeug)
    {
        this.ausleihZeitpunkt = ausleihZeitpunkt;
        this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
        this.fahrzeug = fahrzeug;
    }

    /*_______________getter und setter________________*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getReservierungsDatum() {
        return reservierungsDatum;
    }

    public void setReservierungsDatum(Date reservierungsDatum) {
        this.reservierungsDatum = reservierungsDatum;
    }

    public Timestamp getAusleihZeitpunkt() {
        return ausleihZeitpunkt;
    }

    public void setAusleihZeitpunkt(Timestamp ausleihZeitpunkt) {
        this.ausleihZeitpunkt = ausleihZeitpunkt;
    }

    public Timestamp getRueckgabeZeitpunkt() {
        return rueckgabeZeitpunkt;
    }

    public void setRueckgabeZeitpunkt(Timestamp rueckgabeZeitpunkt) {
        this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
    }

    public Fahrzeug getFahrzeug() {
        return fahrzeug;
    }

    public void setFahrzeug(Fahrzeug fahrzeug) {
        this.fahrzeug = fahrzeug;
    }

    public Benutzer getKunde() {
        return kunde;
    }

    public void setKunde(Benutzer kunde) {
        this.kunde = kunde;
    }
}
