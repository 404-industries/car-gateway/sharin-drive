package de.hsb.sharindrive.model;

import java.sql.Timestamp;

/**
 * Diese Klasse dient nur dazu dass man eine liste
 * der bereits verbuchten Zeiten für ein Fahrzeug
 * einsehen kann.
 */
public class ReservierteZeit
{
    private Timestamp ausleihZeitpunkt;
    private Timestamp rueckgabeZeitpunkt;

    public ReservierteZeit(Timestamp ausleihZeitpunkt, Timestamp rueckgabeZeitpunkt) {
        this.ausleihZeitpunkt = ausleihZeitpunkt;
        this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
    }

    public boolean isMoeglicherAusleihzeitraum(Timestamp ausleih, Timestamp rueckgabe)
    {
        return ausleih.before(ausleihZeitpunkt) && rueckgabe.before(ausleihZeitpunkt) ||
                ausleih.after(ausleihZeitpunkt) && rueckgabe.after(ausleihZeitpunkt);
    }

    /*_______________get und set________________*/
    public Timestamp getAusleihZeitpunkt() {
        return ausleihZeitpunkt;
    }

    public void setAusleihZeitpunkt(Timestamp ausleihZeitpunkt) {
        this.ausleihZeitpunkt = ausleihZeitpunkt;
    }

    public Timestamp getRueckgabeZeitpunkt() {
        return rueckgabeZeitpunkt;
    }

    public void setRueckgabeZeitpunkt(Timestamp rueckgabeZeitpunkt) {
        this.rueckgabeZeitpunkt = rueckgabeZeitpunkt;
    }
}
