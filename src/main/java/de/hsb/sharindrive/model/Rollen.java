package de.hsb.sharindrive.model;

public enum Rollen {
    GAST, ADMINISTRATOR, MITGLIEDER, MITARBEITER
}
