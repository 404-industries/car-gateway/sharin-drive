package de.hsb.sharindrive.service;

import de.hsb.sharindrive.exceptions.ZeitraumBelegtException;
import de.hsb.sharindrive.model.ReservierteZeit;
import de.hsb.sharindrive.model.Reservierung;
import de.hsb.sharindrive.repository_interfaces.ReservierungRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReservierungsService
{
    private final ReservierungRepository reservierungRepository;


    public ReservierungsService(ReservierungRepository reservierungRepository) {
        this.reservierungRepository = reservierungRepository;
    }

    public void reservierungHinzufuegen(Reservierung reservierung)
            throws ZeitraumBelegtException
    {

        //checkt ob fahrzeug zu dem zeitpunkt schon reserviert ist
        if(isReserviert(reservierung))
            throw new ZeitraumBelegtException();
        //sonst hinzufügen
        else
            reservierungRepository.save(reservierung);
    }

    public Reservierung getReservierung(Long id)
    {
        return reservierungRepository.getById(id);
    }

    //stornierungsmethoden könnten eventuell nicht funktionieren wegen repo methoden
    //dann knapp umschreiben
    public void reservierungStornieren(long id)
    {
        reservierungRepository.deleteById(id);
    }

    public void reservierungStornieren(Reservierung reservierung)
    {
        reservierungRepository.delete(reservierung);
    }

    public List<Reservierung> getReservierungen()
    {
        return reservierungRepository.findAll();
    }

    public List<Reservierung> getReservierungenNachKunde(int kundenId)
    {
        List<Reservierung> resKunde = new ArrayList<>();
        for (Reservierung reservierung : reservierungRepository.findAll())
        {
            if (reservierung.getKunde().getId() == kundenId)
            {
                resKunde.add(reservierung);
            }
        }
        return resKunde;
    }

    public List<Reservierung> getReservierungenNachFahrzeug(long fahrzeugId)
    {
        List<Reservierung> resFahrzeug = new ArrayList<>();
        for (Reservierung reservierung : reservierungRepository.findAll())
        {
            if (reservierung.getFahrzeug().getId() == fahrzeugId)
            {
                resFahrzeug.add(reservierung);
            }
        }
        return resFahrzeug;
    }


    /*________________Hilfmethoden_________________*/

    public boolean isReserviert(Reservierung reservierung)
    {
        for(ReservierteZeit resZeit : getReservierteZeiten(reservierung.getFahrzeug().getId()))
        {
            if(!resZeit.isMoeglicherAusleihzeitraum(reservierung.getAusleihZeitpunkt(), reservierung.getRueckgabeZeitpunkt()))
                return true;
        }
        return false;
    }

    public List<ReservierteZeit> getReservierteZeiten(long fahrzeugId)
    {
        List<ReservierteZeit> reserviertListe = new ArrayList<>();

        for (Reservierung reservierung : getReservierungenNachFahrzeug(fahrzeugId))
        {
            //holt die ausleihzeit aus den bereits betätigten reservierungen und
            //erstellt hieraus eine liste
            reserviertListe.add(new ReservierteZeit(reservierung.getAusleihZeitpunkt(),reservierung.getRueckgabeZeitpunkt()));
        }

        return reserviertListe;
    }
}
