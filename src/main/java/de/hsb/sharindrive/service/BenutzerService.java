package de.hsb.sharindrive.service;

import de.hsb.sharindrive.model.Benutzer;
import de.hsb.sharindrive.repository_interfaces.BenutzerRepository;
import org.springframework.stereotype.Service;

@Service
public class BenutzerService
{
    private final BenutzerRepository benutzerRepository;

    public BenutzerService(BenutzerRepository benutzerRepository) {
        this.benutzerRepository = benutzerRepository;
    }

    public Benutzer getBenutzer(int id)
    {
        for(Benutzer benutzer : benutzerRepository.findAll())
        {
            if(benutzer.getId() == id)
                return benutzer;
        }
        return null;
    }

    public void benutzerHinzufuegen(Benutzer b){
        benutzerRepository.save(b);
    }
}
