package de.hsb.sharindrive.service;

import de.hsb.sharindrive.exceptions.IdNonExistantException;
import de.hsb.sharindrive.model.Fahrzeug;
import de.hsb.sharindrive.model.Standort;
import de.hsb.sharindrive.repository_interfaces.FahrzeugRepository;
import de.hsb.sharindrive.repository_interfaces.StandortRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FahrzeugService
{
    private final FahrzeugRepository fahrzeugRepository;
    private final StandortRepository standortRepository;

    @Autowired
    public FahrzeugService(FahrzeugRepository fahrzeugRepository, StandortRepository standortRepository) {
        this.fahrzeugRepository = fahrzeugRepository;
        this.standortRepository = standortRepository;
    }

    public void fahrzeugHinzufuegen(Fahrzeug fahrzeug)
    {

        fahrzeugRepository.save(fahrzeug);
    }

    public ResponseEntity<String> fahrzeugEntfernen(long fahrzeugId)
            throws IdNonExistantException
    {
        if(fahrzeugRepository.existsById(fahrzeugId))
            throw new IdNonExistantException();
        else
        {
            fahrzeugRepository.deleteById(fahrzeugId);
            return (ResponseEntity<String>) ResponseEntity.status(HttpStatus.OK);
        }
    }

    public void fahrzeugTabelleLeeren()
    {
        fahrzeugRepository.deleteAll();
    }

    public Fahrzeug getFahrzeug(long id)
            throws IdNonExistantException
    {
        Fahrzeug searchedFahrzeug = new Fahrzeug();

        if(!fahrzeugRepository.existsById(id))
            throw new IdNonExistantException();
        else
        {
            for (Fahrzeug Fahrzeug : fahrzeugRepository.findAll())
            {
                if (Fahrzeug.getId() == id)
                {
                    searchedFahrzeug = Fahrzeug;
                    break;
                }
            }
            return searchedFahrzeug;
        }
    }

    public List<Fahrzeug> getFahrzeuge()
    {
        return fahrzeugRepository.findAll();
    }

    public List<Fahrzeug> getFahrzeugeNachMarke(String marke)
    {
        List<Fahrzeug> ergebnis = new ArrayList<>();

        for(Fahrzeug fahrzeug : fahrzeugRepository.findAll())
        {
            if(fahrzeug.getMarke() == marke)
                ergebnis.add(fahrzeug);
        }
        return ergebnis;
    }

    public List<Fahrzeug> getFahrzeugeNachStandort(Standort standort)
    {
        List<Fahrzeug> ergebnis = new ArrayList<>();

        for(Fahrzeug fahrzeug : fahrzeugRepository.findAll())
        {
            if(fahrzeug.getStandort() == standort)
                ergebnis.add(fahrzeug);
        }
        return ergebnis;
    }

    public List<Fahrzeug> getFahrzeugeNachGroesse(String groesse)
    {
        List<Fahrzeug> ergebnis = new ArrayList<>();

        for(Fahrzeug fahrzeug : fahrzeugRepository.findAll())
        {
            if(groesse.equals(groesse))
                ergebnis.add(fahrzeug);
        }
        return ergebnis;
    }

    public void standortSetzenNachFahrzeug(Fahrzeug fahrzeug, Standort neuerStandort)
    {
        fahrzeugRepository.getById(fahrzeug.getId()).setStandort(neuerStandort);
    }

    public void standortSetzenNachId(Long fahrzeugId, Standort neuerStandort)
    {
        fahrzeugRepository.getById(fahrzeugId).setStandort(neuerStandort);
    }
}
