# Sharin Drive
Gruppe/Team:        1/2

Teilnehmer: Sebastian Schramm(5130483), Christoph Senft(5155512),
Joel Pitzler(5128668), Rene Breuning(5153703), Maximilian Henke(5129157)

Abgabedatum: 25.07.2022

Laborleitung /-Betreuung:  Prof. Dr.-Ing. Jasminka Matevska <br>
Noah Raven


## Getting started

Dies sind die Projekt-Dateien für das Carsharing Projekt in dem Modul SOFTW2 an der Hochschule Bremen.
Das Projekt erstellt einen Webserver mittels des Spring Boot Framework.
Sharin Drive wurde entwickelt von Sebastian Schramm, Joel Pitzler, Rene Breuning, Maximillian Henke, Christoph Senft.

## Used technologies
- Java 17 (jdk)
- Spring Boot Framework
- Docker
- Postgres (Datenbank)
- Maven
- Bootstrap
- JUnit
- Mockito
- Lombok
- Hibernate

## What you need

### For Linux user:

- [ ] Git
- [ ] jdk17-openjdk
- [ ] Docker
- [ ] Docker-compose

### For Windows User:
- [ ] Git
- [ ] jdk17-openjdk
- [ ] [Docker-Desktop](https://www.docker.com/products/docker-desktop/)
- [ ] WSL-2 (Windows Sublayer for Linux)

#### Notes
Es ist zu beachten, dass WSL-2 installiert ist damit Docker-Desktop auf dem System funktioniert.
Link zur offiziellen Microsoft Installationsanleitung: https://docs.microsoft.com/de-de/windows/wsl/install

## Clone the Project

```
git clone https://git.informatik.hs-bremen.de/jpitzler/Carsharing-Project.git
cd Carsharing-Project
```
or
```
git clone https://gitlab.com/404-industries/car-gateway/sharin-drive.git
cd sharin-drive
```

## Start Spring Boot and Postgres
Bevor man das Programm startet, sollte Docker Desktop bzw. der Docker Service gestartet werden.
Um die Anwendung zu starten, muss im Wurzelverzeichnis folgender Befehl ausführt werden:
```
./mvnw clean package
docker-compose up -d
```
Zum Stoppen der Anwendung:
```
docker-compose down
```
Datenbankdaten werden hierdurch nicht gelöscht. Sollten die gespeicherten Daten ebenfalls gelöscht werden, muss folgendes ausgeführt werden:
```
docker-compose down -v
```

## Use the Project
Um auf die Webseite zugreifen zu können, sollte in einem Browser (z.B. Firefox oder Google Chrome) folgende Adresse eigegeben werden:
localhost:8080/ 
Als Standard Benutzer wird beim Projektstart der Benutzer Admin angelegt. Die Anmeldedaten sind: 
Email: admin@admin.de Passwort: admin. 

## Troubleshooting
Falls es zu Problemen bei der Anmeldung kommt ist es zu empfehlen, die Cookies für die Webseite zu löschen. 
Hierfür als Beispiel im Firefox Browser: Drücke F12 &rarr; Web-Speicher &rarr; Rechtsklick auf Webseiten Link &rarr; Alles Löschen

## Testing
Anschließend können die Unit-, Mutations-, und Coverage-Tests im Wurzelverzeichnis ausgeführt werden mit:
```
mvn clean verify
```
***